import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('https://www.katalon.com/')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
selenium.open("http://localhost:3000/userlistsupplier")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='Kirim Notifikasi'])[1]/following::span[1]")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='Kirim Notifikasi'])[1]/following::span[1]")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='Pengguna'])[1]/following::span[1]")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='Pengguna'])[1]/following::span[1]")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='DAFTAR BUYER'])[1]/following::button[1]")
selenium.click("name=email")
selenium.click("name=email")
selenium.doubleClick("name=email")
selenium.type("name=email", "fa284@outlook.com")
selenium.click("name=password")
selenium.type("name=password", "Admin123")
selenium.type("name=password_confirmation", "Admin123")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='BATAL'])[1]/following::button[1]")
selenium.click("name=name")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='SELANJUTNYA'])[1]/following::div[3]")
selenium.type("name=name", "Andi test")
selenium.click("name=phone")
selenium.click("name=phone")
selenium.doubleClick("name=phone")
selenium.type("name=phone", "09987654378")
selenium.click("name=address")
selenium.type("name=address", "Jalan jalan ke situ aja")
selenium.click("id=exampleSelect")
selenium.select("id=exampleSelect", "label=BANTEN")
selenium.click("name=city")
selenium.select("name=city", "label=KABUPATEN PANDEGLANG")
selenium.click("name=district")
selenium.select("name=district", "label=CIBALIUNG")
selenium.click("name=Kelurahan")
selenium.select("name=Kelurahan", "label=SUDIMANIK")
selenium.click("name=codepos")
selenium.type("name=codepos", "13289")
selenium.click("name=bank")
selenium.select("name=bank", "label=Bank Central Asia")
selenium.click("name=accountNumber")
selenium.type("name=accountNumber", "12345678")
selenium.click("name=accountOwner")
selenium.type("name=accountOwner", "Jakaarta utara")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='BATAL'])[2]/following::button[1]")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='Pin Point Alamat'])[1]/following::div[12]")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='No. NPWP'])[1]/following::label[1]")
selenium.type("id=npwpImage", "C:\\fakepath\\1. colorado.jpg")
selenium.click("name=npwp")
selenium.type("name=npwp", "123456789012345")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='file'])[1]/following::p[1]")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='No. NPWP'])[1]/following::button[1]")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='Upload'])[1]/img[1]")
selenium.type("id=npwpImage", "C:\\fakepath\\1. colorado.jpg")
selenium.click("name=ktp")
selenium.type("name=ktp", "1234567890123456")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='No. KTP'])[1]/following::label[1]")
selenium.type("id=ktpImage", "C:\\fakepath\\1. colorado.jpg")
selenium.click("name=siup")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='No. SIUP'])[1]/following::label[1]")
selenium.type("id=siupImage", "C:\\fakepath\\1. colorado.jpg")
selenium.click("name=siup")
selenium.type("name=siup", "1234567890123456")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='Uploaded'])[3]/following::p[2]")
selenium.click("xpath=(.//*[normalize-space(text()) and normalize-space(.)='BATAL'])[3]/following::button[1]")
